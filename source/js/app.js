// Code goes here

var mainApp = angular.module('mainApp', ['ui.router', 'ngMaterial', 'mainApp.signInController']);

mainApp.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

  $stateProvider

    // HOME STATES AND NESTED VIEWS ========================================
    .state('signIn', {
      url: '/signIn',
      templateUrl: 'views/signIn.html',
      controller: 'signInCtrl'
    })

    // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
    .state('about', {
      url: '/about',
      template: '<div layout="column" flex layout-align="center center"><h3>Its the UI-Router hello world app!</h3></div>'
    });

  //  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/signIn');

});

mainApp.controller('mainContr', function($rootScope, $scope, $state, $mdSidenav) {
  $rootScope.profile = {
    pic: "",
    email: "",
    name: ""
  };

  $rootScope.check = function() {
    $scope.hideSideNav = $rootScope.hideSideNav;
    $scope.hideToolBar = $rootScope.hideToolBar;
  };

  $rootScope.init = function() {
    gapi.load('auth2', function() {
      var GoogleAuth = gapi.auth2.getAuthInstance();
      if (GoogleAuth.isSignedIn.get() === false) {
        $rootScope.hideSideNav = true;
        $rootScope.hideToolBar = true;
      }
    });
  };

  window.init = $rootScope.init;

  $scope.toggleLeft = function() {
    $mdSidenav('left').toggle();
  };

  $scope.goTo = function(state) {
    $state.go(state);
    $mdSidenav('left').toggle();
  };

  $scope.signOut = function() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function() {
      $state.go('signIn');
      console.log('User signed out.');
    });
  };

});