// Code goes here

var mainApp = angular.module('mainApp.signInController', []);

mainApp.controller('signInCtrl', function($rootScope, $scope, $state) {

  $rootScope.hideSideNav = true;
  $rootScope.hideToolBar = true;

  $rootScope.check();

  function onSuccess(googleUser) {
    console.log(googleUser.getAuthResponse().id_token);
    var profile = googleUser.getBasicProfile();
    $rootScope.profile.pic = profile.getImageUrl();
    $rootScope.profile.email = profile.getEmail();
    $rootScope.profile.name = profile.getName();

    $rootScope.hideSideNav = false;
    $rootScope.hideToolBar = false;
    $rootScope.check();

    $state.go('about');
  }

  function onFailure(error) {
    console.log(error);
  }

  gapi.signin2.render('my-signin2', {
    'scope': 'profile email',
    'width': 240,
    'height': 50,
    'longtitle': true,
    'theme': 'dark',
    'onsuccess': onSuccess,
    'onfailure': onFailure
  });

});